﻿using System;
using System.IO;
using System.Threading;
using CommandLine;
using VeeamSample.SignatureTool.SignatureComputing;
using VeeamSample.SignatureTool.Utilities;

namespace VeeamSample.SignatureTool
{
    internal static class Program
    {
        private static readonly CancellationTokenSource cancellationTokenSource = new();

        // (usage: see --help option or ProgramOptions class)
        private static void Main(string[] args)
        {
            Console.CancelKeyPress += (_, _) => cancellationTokenSource.Cancel();
            try
            {
                Parser.Default
                    .ParseArguments<ProgramOptions>(args)
                    .WithParsed(RunTool);
            }
            catch (Exception exception)
            {
                DefaultExceptionHandler.Handle(exception);
            }
        }

        private static void RunTool(ProgramOptions options)
        {
            var tryRunningUnbuffered = options.ShouldTryRunUnbuffered ?? true;
            var maxWorkers = options.MaxWorkers ?? Environment.ProcessorCount;
            var blockSize = options.BlockSize;
            var cancellationToken = cancellationTokenSource.Token;
            
            using var outputQueue = new ConsoleMessageQueue();
            var blockHandler = new BlockHashComputingHandler(outputQueue);
            var blockwiseProcessor = new StreamBlockwiseProcessor(blockHandler);
            using Stream fileStream = FileUtilities.OpenFileForReading(options.FilePath, blockSize, tryRunningUnbuffered);

            blockwiseProcessor.Process(fileStream, blockSize, maxWorkers, cancellationToken);
        }
    }
}