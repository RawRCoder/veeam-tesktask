﻿namespace VeeamSample.SignatureTool.Utilities.Extensions
{
    internal static class IntegerExtensions
    {
        public static bool IsMultiplyOf(this int a, int b)
            => a % b == 0;
    }
}