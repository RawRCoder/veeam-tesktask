using System.Collections.Generic;

namespace VeeamSample.SignatureTool.Utilities.Extensions
{
    internal static class StringExtensions
    {
        public static string JoinToString<T>(this IEnumerable<T> collection, string? separator = null)
            => string.Join(separator, collection);
    }
}