using System;
using System.IO;
using System.Runtime.InteropServices;
using VeeamSample.SignatureTool.Utilities.Extensions;

namespace VeeamSample.SignatureTool.Utilities
{
    internal static class FileUtilities
    {
        public static FileStream OpenFileForReading(string filePath, int blockSize, bool shouldTryRunningUnbuffered)
        {
            const FileOptions FILE_FLAG_NO_BUFFERING = (FileOptions) 0x20000000;
            // I could try to find a way to get actual sector size for specified drive.
            // But I guess that will be to much of unnecessary code with tons of caveats.  
            const int MAX_TYPICAL_SECTOR_SIZE = 4096;
            const int MIN_BUFFER_SIZE = MAX_TYPICAL_SECTOR_SIZE;
            
            // Turning buffering off so reading will utilize hard drive as much as possible.
            // However this potentially worsens the execution time by several hundreds of % in some cases (ie when file is system cache entirely).  
            // Also this is implemented only for windows. They say there are some ways around on MacOS and other systems.
            // But I decided not to bother myself with things I can hardly debug (since I got no MacOS installation).
            var isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
            var shouldRunUnbufferedOnWindows = shouldTryRunningUnbuffered && isWindows && blockSize.IsMultiplyOf(MAX_TYPICAL_SECTOR_SIZE);
            var fileOptions = (shouldRunUnbufferedOnWindows ? FILE_FLAG_NO_BUFFERING : FileOptions.None)
                              | FileOptions.SequentialScan;
            
            var bufferSize = Math.Max(blockSize, MIN_BUFFER_SIZE);
            
            return new FileStream(filePath, 
                FileMode.Open, 
                FileAccess.Read, 
                FileShare.Read,
                bufferSize, 
                fileOptions);
        }
    }
}