using System;
using System.Diagnostics.CodeAnalysis;

namespace VeeamSample.SignatureTool.Utilities
{
    internal static class DefaultExceptionHandler
    {
        [DoesNotReturn]
        public static void Handle(Exception exception)
        {
            var message = $"Unhandled exception ({exception.GetType().Name}) - {exception.Message}\n"
                          + exception.StackTrace;
            Console.Error.WriteLine(message);
            Environment.Exit(exitCode: 1);
        }
    }
}