namespace VeeamSample.SignatureTool.Utilities
{
    public interface IMessageQueue
    {
        void Enqueue(string message);
    }
}