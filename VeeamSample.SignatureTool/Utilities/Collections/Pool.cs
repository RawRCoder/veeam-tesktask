using System;
using System.Collections.Concurrent;
using ConcurrentCollections;

namespace VeeamSample.SignatureTool.Utilities.Collections
{
    internal sealed partial class Pool<T>
    {
        // Perhaps I should have implemented IDisposable and (try to) dispose of all of the items
        // For the sake of simplicity I decided not to do this

        private readonly ConcurrentQueue<Item> freeItems = new();
        // Probably could have used the standard ConcurrentDictionary, HashSet just feels a cleaner way to do this
        private readonly ConcurrentHashSet<Item> usedItems = new();
        private readonly Func<T> dataFactory;

        public Pool(Func<T> dataFactory)
        {
            this.dataFactory = dataFactory;
        }
        
        public Item Get()
        {
            if (freeItems.TryDequeue(out Item? item))
            {
                usedItems.Add(item);
                return item;
            }

            var data = dataFactory.Invoke();
            var freshItem = new Item(this, data);
            usedItems.Add(freshItem);
            return freshItem;
        }

        public bool Return(Item item)
        {
            if (!usedItems.TryRemove(item))
                return false;

            freeItems.Enqueue(item);
            return true;
        }
        
        // Public interface of this Pool could be richer, like FreeItemsCount or sth.
        // But I just don't need anything else for now
    }
}