namespace VeeamSample.SignatureTool.Utilities.Collections
{
    internal sealed partial class Pool<T>
    {
        // Item is just a T-wrapper. I am still not 100% certain if I really need this.
        // It helps not to worry about possibility of T having messed up comparisons/hash functions.
        // Also it helps to distinguish own items vs other pools' items. Releasing alien items in a pool is unwanted
        public sealed class Item
        {
            private readonly Pool<T> ownerPool;
            public Item(Pool<T> ownerPool, T data)
            {
                this.ownerPool = ownerPool;
                Data = data;
            }
            
            public T Data { get; }

            /// <summary>
            /// Returns this item to owner pool.
            /// Usage of <see cref="Data"/> is potentially unsafe after this call 
            /// </summary>
            public void Discard()
                => ownerPool.Return(this);
        };
    }
}