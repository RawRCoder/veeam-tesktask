using System;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;

namespace VeeamSample.SignatureTool.Utilities
{
    /// <summary>
    /// Console output done as a separate async queue
    /// Console operations could block thread execution for a significant time
    /// So a separate thread with a queue consumer is used
    /// </summary>
    internal sealed class ConsoleMessageQueue : IDisposable, IMessageQueue
    {
        private readonly ConcurrentQueue<string> messageQueue = new();
        private readonly AutoResetEvent queueEvent = new(false);
        private readonly CancellationTokenSource cancellationTokenSource = new();
        private readonly Thread thread;

        public ConsoleMessageQueue()
        {
            thread = new Thread(ThreadLoop)
            {
                Name = nameof(ConsoleMessageQueue)
            };
            thread.Start();
        }

        public void Enqueue(string message)
        {
            messageQueue.Enqueue(message);
            queueEvent.Set();
        }

        private void ThreadLoop()
        {
            var token = cancellationTokenSource.Token;
            var messagesBundleBuilder = new StringBuilder();
            
            while (token.IsCancellationRequested is false)
            {
                queueEvent.WaitOne();
                while (messageQueue.TryDequeue(out string? message))
                {
                    messagesBundleBuilder.AppendLine(message);
                }

                Console.Write(messagesBundleBuilder.ToString());
                messagesBundleBuilder.Clear();
            }
        }

        public void StopProcessing()
        {
            cancellationTokenSource.Cancel();
            queueEvent.Set();
            thread.Join();
        }

        public void Dispose()
        {
            StopProcessing();
            queueEvent.Dispose();
        }
    }
}