using CommandLine;

namespace VeeamSample.SignatureTool
{
    public class ProgramOptions
    {
        [Value(0, Required = true, MetaName = "File path")]
        public string FilePath { get; set; } = default!;

        // Would prefer making this an option, but I've assumed that some automated testing might be involved
        [Value(1, Required = true, MetaName = "Block size in bytes")]
        public int BlockSize { get; set; }

        [Option('t', "threads", 
            HelpText = "Number of parallel threads/workers to use. ProcessorCount is used by default")]
        public int? MaxWorkers { get; set; }

        [Option('u', "unbuffered", 
            HelpText = "Instructs not to use OS file cache on Windows (size of block should be a multiple of 4096")]
        public bool? ShouldTryRunUnbuffered { get; set; }
    }
}