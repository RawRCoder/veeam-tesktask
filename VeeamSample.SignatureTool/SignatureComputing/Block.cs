using System;
using VeeamSample.SignatureTool.Utilities.Collections;

namespace VeeamSample.SignatureTool.SignatureComputing
{
    internal record Block(long Id, Pool<byte[]>.Item BufferPoolItem, int DataSize) : IDisposable
    {
        public void Dispose()
        {
            BufferPoolItem.Discard();
        }
    }
}