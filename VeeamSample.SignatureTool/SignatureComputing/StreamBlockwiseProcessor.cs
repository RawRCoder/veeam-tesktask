using System.IO;
using System.Threading;
using VeeamSample.SignatureTool.Utilities.Collections;

namespace VeeamSample.SignatureTool.SignatureComputing
{
    internal class StreamBlockwiseProcessor
    {
        private readonly IBlockHandler blockHandler;
        public StreamBlockwiseProcessor(IBlockHandler blockHandler)
        {
            this.blockHandler = blockHandler;
        }

        /// <summary>
        /// Reads stream sequentially, block by block.
        /// Then asynchronously handles a block with a <see cref="IBlockHandler"/>
        /// </summary>
        /// <param name="sourceStream">Stream to read</param>
        /// <param name="blockSize">The size of each block (except last one)</param>
        /// <param name="maxWorkers">Max workers to use. Pass null to use ProcessorCount</param>
        /// <param name="cancellationToken"></param>
        public void Process(Stream sourceStream, int blockSize, int maxWorkers, CancellationToken cancellationToken)
        {
            // Pool is used so that LOH won't be filled with (possibly) tons of short-living objects
            var bufferPool = new Pool<byte[]>(() => new byte[blockSize]);
            using var jobDispatcher = new BlockHandlingTaskDispatcher(blockHandler, maxWorkers);

            for (long currentBlockId = 0; cancellationToken.IsCancellationRequested is false; ++currentBlockId)
            {
                Pool<byte[]>.Item bufferItem = bufferPool.Get();
                byte[] buffer = bufferItem.Data;
                int bytesRead = sourceStream.Read(buffer, 0, buffer.Length);
                

                if (bytesRead <= 0)
                    break;

                var block = new Block(currentBlockId, bufferItem, bytesRead);
                jobDispatcher.DispatchBlockHandlingTask(block, cancellationToken);
            }
        }
    }
}