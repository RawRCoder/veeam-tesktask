using System;
using System.Threading;
using VeeamSample.SignatureTool.Utilities;

namespace VeeamSample.SignatureTool.SignatureComputing
{
    internal sealed partial class BlockHandlingTaskDispatcher
    {
        private class Worker : IDisposable
        {
            private readonly BlockHandlingTaskDispatcher dispatcher;
            private readonly IBlockHandler blockHandler;
            private readonly AutoResetEvent newTaskEvent = new(false);
            private readonly CancellationTokenSource ownCancellation = new();
            
            // (Could probably use a ConcurrentQueue here as a safer option,
            //    however I decided it will induce unnecessary overhead)
            private Block? nextBlockToHandle;

            public Worker(int id, BlockHandlingTaskDispatcher dispatcher, IBlockHandler blockHandler)
            {
                this.dispatcher = dispatcher;
                this.blockHandler = blockHandler;
                Id = id;
            }

            public int Id { get; }
            public bool Started => Thread != null;
            private Thread? Thread { get; set; }

            public void Start(CancellationToken token)
            {
                if (Started)
                    throw new Exception("Already started");

                var thread = new Thread(ThreadLoop)
                {
                    Name = $"Block handling worker (#{Id})"
                };
                thread.Start();
                Thread = thread;


                void ThreadLoop()
                {
                    try
                    {
                        var ownToken = ownCancellation.Token;
                        while (token.IsCancellationRequested is false && ownToken.IsCancellationRequested is false)
                        {
                            newTaskEvent.WaitOne();
                            {
                                // Normally nextBlockToHandle can only be changed by HandleBlock or Terminate.
                                // First one is only invoked for free workers, that is - not at this point.
                                // If second one is called - that's ok, we handle that
                                var block = nextBlockToHandle;
                                if (block is null)
                                    continue;

                                blockHandler.Handle(block);
                            }
                            dispatcher.OnWorkerFinishedHandling(worker: this);
                        }
                    }
                    catch (Exception exception)
                    {
                        DefaultExceptionHandler.Handle(exception);
                    }
                }
            }

            public void HandleBlock(Block block)
            {
                nextBlockToHandle = block;
                newTaskEvent.Set();
            }

            public void Terminate()
            {
                ownCancellation.Cancel();
                nextBlockToHandle = null;
                newTaskEvent.Set();
                Thread?.Join();
            }

            public void Dispose()
            {
                newTaskEvent.Dispose();
                ownCancellation.Dispose();
            }
        }
    }
}