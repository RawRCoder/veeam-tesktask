using System.Linq;
using System.Security.Cryptography;
using VeeamSample.SignatureTool.Utilities;
using VeeamSample.SignatureTool.Utilities.Extensions;

namespace VeeamSample.SignatureTool.SignatureComputing
{
    internal class BlockHashComputingHandler : IBlockHandler
    {
        private readonly IMessageQueue outputMessageQueue;
        public BlockHashComputingHandler(IMessageQueue outputMessageQueue)
        {
            this.outputMessageQueue = outputMessageQueue;
        }

        public void Handle(Block block)
        {
            (byte[] signatureBytes, long blockId) = GetBlockSignature(block);

            var signatureString = signatureBytes
                .Select(b => b.ToString("X2"))
                .JoinToString();

            outputMessageQueue.Enqueue($"{signatureString} - block #{blockId}");
        }

        private static (byte[] signatureBytes, long blockId) GetBlockSignature(Block block)
        {
            using (block)
            {
                var sourceBytes = block.BufferPoolItem.Data;
                using SHA256 sha256 = SHA256.Create();
                var signatureBytes = sha256.ComputeHash(sourceBytes, 0, block.DataSize);
                return (signatureBytes, block.Id);
            }
        }
    }
}