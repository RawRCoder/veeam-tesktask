using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace VeeamSample.SignatureTool.SignatureComputing
{
    internal sealed partial class BlockHandlingTaskDispatcher : IDisposable
    {
        private readonly List<Worker> allWorkers;
        private readonly object allWorkersListLock = new();

        // Stack instead of queue so that fewer thread switches could occur
        private readonly ConcurrentStack<Worker> freeWorkers = new();
        private readonly SemaphoreSlim workersSemaphore;
        private readonly CancellationTokenSource cancellationSource = new();
        private readonly IBlockHandler blockHandler;
        private bool maxWorkersCreated;

        public BlockHandlingTaskDispatcher(IBlockHandler blockHandler, int maxWorkers)
        {
            this.blockHandler = blockHandler;
            workersSemaphore = new SemaphoreSlim(maxWorkers, maxWorkers);
            allWorkers = new List<Worker>(maxWorkers);
        }

        public void DispatchBlockHandlingTask(Block block, CancellationToken cancellationToken)
        {
            if (!TryGetWorker(out Worker? worker, cancellationToken) || cancellationToken.IsCancellationRequested)
                return;

            worker.HandleBlock(block);

            if (worker.Started is false)
                worker.Start(cancellationSource.Token);
        }

        public void TerminateAll()
        {
            cancellationSource.Cancel();
            lock (allWorkersListLock)
            {
                foreach (Worker worker in allWorkers)
                    worker.Terminate();
            }
        }

        public void Dispose()
        {
            TerminateAll();
            lock (allWorkersListLock)
            {
                foreach (Worker worker in allWorkers)
                    worker.Dispose();
            }
            workersSemaphore.Dispose();
            cancellationSource.Dispose();
        }

        private bool TryGetWorker([NotNullWhen(true)] out Worker? worker, CancellationToken cancellationToken)
        {
            while (cancellationToken.IsCancellationRequested is false)
            {
                workersSemaphore.Wait(cancellationToken);

                if (freeWorkers.TryPop(out worker))
                    return true;

                if (maxWorkersCreated) 
                    continue;
                
                lock (allWorkersListLock)
                {
                    if (allWorkers.Count < allWorkers.Capacity)
                    {
                        worker = CreateNewWorker();
                        return true;
                    }
                    
                    maxWorkersCreated = true;
                }
            }

            worker = default;
            return false;
        }

        private Worker CreateNewWorker()
        {
            var worker = new Worker(allWorkers.Count, this, blockHandler);
            allWorkers.Add(worker);
            return worker;
        }

        private void OnWorkerFinishedHandling(Worker worker)
        {
            freeWorkers.Push(worker);
            workersSemaphore.Release();
        }
    }
}