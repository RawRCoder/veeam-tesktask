namespace VeeamSample.SignatureTool.SignatureComputing
{
    internal interface IBlockHandler
    {
        void Handle(Block block);
    }
}